#define CATCH_CONFIG_MAIN

#include <sstream>

#include "catch.hpp"
#include "../dialButtons/utils.h"

TEST_CASE("Test utils")
{
	std::stringstream in;
	std::stringstream out;

	auto reset = [&in, &out]()
	{
		in.str("");
		in.clear();
		out.str("");
		out.clear();
	};
	
	// Test "hi"
	in << "1\nhi\n";	
	REQUIRE(GenerateSequenceForStringList(in, out));
	out.seekg(0);
	REQUIRE(out.str() == "Case #1: 44 444\n");

	// Incorrect number of cases: 2 but actually there is only one case
	reset();
	in << "2\nhi\n";
	REQUIRE(!GenerateSequenceForStringList(in, out));

	// Incorrect number of cases: provide "a" which is not a number
	reset();
	in << "a\n\n";
	REQUIRE(!GenerateSequenceForStringList(in, out));
	
	// Incorrect character: '1' which is less than 'a'
	reset();
	in << "1\nZ\n";
	REQUIRE(!GenerateSequenceForStringList(in, out));
	out.seekg(0);
	REQUIRE(out.str() == "Case #1: ");

	// Incorrect character: '{' which is bigger than 'z'
	reset();
	in << "1\n{\n";
	REQUIRE(!GenerateSequenceForStringList(in, out));
	out.seekg(0);
	REQUIRE(out.str() == "Case #1: ");

	// Case #1: one whitespace
	// Case #2: test two whitespaces
	reset();
	in << "2\n \nfoo  bar\n";
	REQUIRE(GenerateSequenceForStringList(in, out));
	out.seekg(0);
	REQUIRE(out.str() == "Case #1: 0\nCase #2: 333666 6660 022 2777\n");

	// All characters
	reset();
	in << "1\nabcdefghijklmnopqrstuvwxyz \n";
	REQUIRE(GenerateSequenceForStringList(in, out));
	out.seekg(0);
	REQUIRE(out.str() == "Case #1: 2 22 2223 33 3334 44 4445 55 5556 66 6667 77 777 77778 88 8889 99 999 99990\n");

	// Test max-length string
	reset();
	in << "1\n";
	for (int i = 0; i < 1000; ++i)
	{
		in << "z";
	}
	in << "\n";
	REQUIRE(GenerateSequenceForStringList(in, out));
	
	// Test a string whose length exceeds the maximum
	reset();
	in << "1\n";
	for (int i = 0; i < 1000; ++i)
	{
		in << "z";
	}
	in << " \n"; // <- added the whitespace
	REQUIRE(!GenerateSequenceForStringList(in, out));

	// Test empty string
	reset();
	in << "1\n\n";
	REQUIRE(GenerateSequenceForStringList(in, out));
	REQUIRE(out.str() == "Case #1: \n");
}

﻿#include <iostream>
#include "utils.h"

int main()
{
	return GenerateSequenceForStringList(std::cin, std::cout) ? 0 : -1;
}

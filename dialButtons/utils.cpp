#include "utils.h"

#include <sstream>
#include <string>
#include <vector>

void Log(const std::string& sMessage)
{
	std::cerr << sMessage << std::endl;
}

/**
 * Calculates the number of keypresses to get the given character
 * @param [IN] cChar          Character for which the number of keypresses is
 *                            to be calculated
 * @param [IN] cFirstChar     First character on the dial pad button related to
 *                            the given character
 * @param [IN] nCharsOnButton Number of characters on the dial pad button related
 *                            to the given character
 * @return the number of keypresses to get the given character
 */
static int GetPresses(char cChar, char cFirstChar, int nCharsOnButton)
{
	return (cChar - cFirstChar) % nCharsOnButton + 1;
}

/**
 * Determines a dial pad button for the given character,
 * and determines how many times the dial pad button should be pressed
 * to get the character
 *
 * @param [IN] cChar Character for which a dial pad button and number of
 *                   keypresses are to be found
 * @param [OUT] rButton Dial pad button related to the given character
 * @param [OUT] rPresses Number of key presses to get the given character
 * @return true if function succeeds, false otherwise
 */
static bool GetButton(char cChar, int& rButton, int& rPresses)
{
	bool bOK = true;
	rButton = -1;
	rPresses = -1;

	if (cChar != ' ' && !(cChar >= 'a' && cChar <= 'z'))
	{
		std::ostringstream errMsg;
		errMsg << "Unexpected character[" << cChar << "]";
		Log(errMsg.str());
		bOK = false;
	}
	else
	{
		if (cChar == ' ')
		{
			rButton = 0;
			rPresses = 1;
		}
		else if (cChar >= 'a' && cChar <= 'o')
		{
			rButton = (cChar - 'a') / 3 + 2;
			rPresses = GetPresses(cChar, 'a', 3);
		}
		else if (cChar >= 'w')
		{
			rButton = 9;
			rPresses = GetPresses(cChar, 'w', 4);
		}
		else if (cChar >= 't')
		{
			rButton = 8;
			rPresses = GetPresses(cChar, 't', 3);
		}
		else // 'p', 'q', 'r', 's'
		{
			rButton = 7;
			rPresses = GetPresses(cChar, 'p', 4);
		}
	}
	return bOK;
}

/**
 * Generates and outputs to \p rOutput a sequence of keypresses for the given characters
 * @param [IN] sString Characters for which to generate the sequence of keypresses
 * @param [IN] rOutput Output stream
 * @return true if the operation succeeds, false otherwise
 */
static bool GenerateSequenceForString(const std::vector<char>& sString, std::ostream& rOutput)
{
	bool bOK = true;
	int nPrevButton = -1;

	for (int iChar = 0; sString[iChar] != '\0'; ++iChar)
	{
		int nButton = -1;
		int nPresses = -1;

		if (!GetButton(sString[iChar], nButton, nPresses))
		{
			bOK = false;
			break;
		}

		if (nPrevButton == nButton)
		{
			rOutput << " ";
		}

		for (int iPress = 0; iPress < nPresses; ++iPress)
		{
			rOutput << nButton;
		}

		nPrevButton = nButton;
	}

	if (bOK)
	{
		rOutput << std::endl;
	}
	return bOK;
}

bool GenerateSequenceForStringList(std::istream& rInput, std::ostream& rOutput)
{
	bool bOK = true;
	int nStrings = 0;

	rInput >> nStrings;

	if (nStrings < 1)
	{
		std::ostringstream errMsg;
		errMsg << "Invalid number of cases[" << nStrings << "]";
		Log(errMsg.str());
		bOK = false;
	}
	else
	{
		rInput.ignore();

		const int MAX_CHARS = 1000;
		std::vector<char> str(MAX_CHARS + 1 /* one extra byte for '\0' */);

		for (int i = 0; i < nStrings; ++i)
		{
			rInput.getline(str.data(), str.size());

			if (!rInput.good())
			{
				Log("Read operation failed");
				bOK = false;
				break;
			}

			rOutput << "Case #" << (i + 1) << ": ";
			if (!GenerateSequenceForString(str, rOutput))
			{
				std::ostringstream errMsg;
				errMsg << "GenerateSequenceForString() failed. i = " << i;
				Log(errMsg.str());
				bOK = false;
				break;
			}
		}
	}
	return bOK;
}
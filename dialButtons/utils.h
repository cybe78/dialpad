#pragma once

#include <iostream>

/**
 * Generates and outputs to \p rOutput sequence of keypresses to indicate the desired characters
 * provided in \p rInput.
 * For details, see https://code.google.com/codejam/contest/dashboard?c=351101#s=p2
 * @param [IN] rInput Input stream
 * @param [OUT] rOutput Output stream
 * @return true if the operation succeeds, false otherwise
 */
bool GenerateSequenceForStringList(std::istream& rInput, std::ostream& rOutput);

/**
 * Logs the given error message
 * @param [IN] sMessage Error message
 */
void Log(const std::string& sMessage);